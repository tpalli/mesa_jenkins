# gpu hang
KHR-GL46.tessellation_shader.tessellation_shader_tc_barriers.barrier_guarded_read_write_calls
KHR-GL46.shader_image_load_store.basic-api-bind

# too slow on ICL (>30 minutes)
KHR-GL46.arrays_of_arrays_gl.InteractionFunctionCalls2

# flaky on m32
GTF-GL46.gtf32.GL3Tests.sync.sync_functionality_clientwaitsync_flush

# flaky
KHR-GL46.shader_subroutine.control_flow_and_returned_subroutine_values_used_as_subroutine_input
KHR-GL46.sync.flush_commands
KHR-GL46.geometry_shader.limits.max_combined_texture_units
